const React = require("react");
const { renderToString } = require("react-dom/server");
const http = require("http");
const { default: styled } = require("@emotion/styled");
const { default: createCache } = require("@emotion/cache");
const { CacheProvider } = require("@emotion/react");

const port = 3000;

const BaseWrapper = styled.div({
  color: "white",
  fontSize: "3em",
  fontFamily: "system-ui, sans-serif",
  height: "100vh",
  padding: "1rem",
});

const HotPinkWrapper = styled(BaseWrapper)({
  backgroundColor: "hotpink",
});

const TealWrapper = styled(BaseWrapper)({
  backgroundColor: "teal",
});

const key = "custom";
const cache = createCache({ key });

const App = ({ airline }) => {
  const Wrapper = airline === "United" ? TealWrapper : HotPinkWrapper;
  return React.createElement(
    CacheProvider,
    { value: cache },
    React.createElement(
      Wrapper,
      null,
      React.createElement("div", null, airline)
    )
  );
};

const server = http.createServer((req, res) => {
  const html = renderToString(
    React.createElement(App, {
      airline: req.url === "/united" ? "United" : "Everymundo",
    })
  );
  res.writeHead(200, { "Content-Type": "text/html" });
  res.end(
    `<html><style>body { margin: 0; }</style><body>${html}</body></html>`
  );
});

server.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
